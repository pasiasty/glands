//zainstalowac biblioteke armadillo do liczenie macierzowego
//warto dodac sobie wizualizacje plasterkow wolumenu jak w publikacji by zobaczyc zbieznosc - dowolna biblioteka graficzna - ogladanie wynikow liczbowych jest nudne np. SDL
//dodac openmp - tam gdzie podalem - niezbedne bo sie muli
//mpi pozwala na zrowoleglenie ale pytanie co mozna zrowoleglac - najlepiej liczyc osobne propozycje parametrow na kazdym z kompow i co jakis czas (np. 1s synchronizowac najlepszy wariant miedzy nimi, tak by starowac ponownie z najlepszymi)
//mozna tez liczyc bez openmp i wtedy kazdy rdzen to osobny proces MPI (moze byc szybciej

//problem to zbieznosc - im blizej rozwiazania tym trudniej bo jest duzo nieliniowosci
//kluczowe parametry to polozenie i amplitudy oraz beta, boroty sa mniej wazne zwlaszcza przy symetrii (kola zamiast elips)


#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

//rozmiar boku wolumenu
#define    BSIZE 32

//liczba blobow generowanych i syntezowanych (zakladamy ze wiemy ile jest, nie poszukujemy ich liczby automatycznie)
#define    NPAR   3

#define    PI  3.1425926

//opisane dalej
#define    DMIN 3

//ilosc iteracji
#define N_MAX 10000

mat generateParameters(mat SCALE)
{
    mat par   = mat(NPAR,11,fill::zeros);

    arma_rng::set_seed_random();

    for(;;)						//synteza NPAR-blobow do skutku - odleglosc miedzy 
    {
       par = randu<mat>(NPAR,11);			//losujemy parametry [0..1]

       for (int i=0;i<NPAR;i++)				//docinamy do zakresu
          for (int j=0;j<10;j++)
              par(i,j)=fmod(1000.0*par(i,j), SCALE(1,j)-SCALE(0,j)) + SCALE(0,j); //drugi arg. to roznica, SCALE(0,j) to offset

       //synteza jest zrobiona dobrze jesli odleglosc euklidesowa x,y,z miedzy kazda para jest nie mniejsza niz DMIN (inaczej jest zakladkowanie)
       bool good=1;
       for (int i=0;i<NPAR;i++)
          for (int j=i+1;j<NPAR;j++)
          {
              float d = sqrt( pow(par(i,0)-par(j,0),2) + pow(par(i,1)-par(j,1),2) + pow(par(i,2)-par(j,2),2) );
              if (d<DMIN)
                 good=false;
          }

       if (good) break;
    }

    return par;
}

cube calculateVolumen(mat par)
{
    float th, Amp, beta;
    int w,k,z;

    mat Rx   = mat(3,3,fill::zeros);
    mat Ry   = mat(3,3,fill::zeros);
    mat Rz   = mat(3,3,fill::zeros);
    mat R    = mat(3,3);
    mat S    = mat(3,3,fill::zeros);
    mat Sig  = mat(3,3);
    mat Sig1 = mat(3,3);
    mat mi   = mat(3,1);
    mat x    = mat(3,1);
    mat xmi  = mat(3,1);

    cube V   = cube(BSIZE,BSIZE,BSIZE,fill::zeros);

    for (int ipar=0;ipar<NPAR;ipar++)
    {
        Amp  = par(ipar,9);
        beta = par(ipar,10);

        th = par(ipar,3)*2.0*PI/360.0;
        Rx(0,0)  =  1;
        Rx(1,1)  =  cos(th);   Rx(1,2) = -sin(th);
        Rx(2,1)  =  sin(th);   Rx(2,2) =  cos(th);

        th = par(ipar,4)*2.0*PI/360.0;;
        Ry(0,0)  =  cos(th);   Ry(0,2) =  sin(th);
        Ry(1,1)  =  1;
        Ry(2,0)  = -sin(th);   Ry(2,2) =  cos(th);

        th = par(ipar,5)*2.0*PI/360.0;;
        Rz(0,0)  =  cos(th);   Rz(0,1) = -sin(th);
        Rz(1,0)  =  sin(th);   Rz(1,1) =  cos(th);
        Rz(2,2)  =  1;

        R=Rx*Ry*Rz;

        S(0,0) = par(ipar,6);
        S(1,1) = par(ipar,7);
        S(2,2) = par(ipar,8);

        Sig  = R*S*S*R.i();
        Sig1 = Sig.i();
        mi(0,0) = par(ipar,0);
        mi(1,0) = par(ipar,1);
        mi(2,0) = par(ipar,2);

        mat t= mat(1,3);
        mat v= mat(1,1);

        for (w=0;w<BSIZE;w++)	//openmp - zrowoleglic w !
           for (k=0;k<BSIZE;k++)
              for (z=0;z<BSIZE;z++)
              {
                 x(0,0)=w;
                 x(1,0)=k;
                 x(2,0)=z;
                 xmi=x-mi;

                 v = abs(trans(xmi)*Sig1*xmi);
                 v = exp(-pow(v,beta));
                 V(w,k,z) = V(w,k,z) + Amp*v(0,0);
              }
    }
    // V - wolumen po syntezie

    return V;
}

float calculateError(cube c1, cube c2)
{
    int w,k,z;
    float err=0;

    for (w=0;w<BSIZE;w++)	//openmp - zrowoleglic w ! tylko nie robic instrukcji atomowej dla dodawania - ma byc wektor[BSIZE] a potem poza pragma sumujemy wyniki tymczasowe bo sie bedzie mulilo
       for (k=0;k<BSIZE;k++)
          for (z=0;z<BSIZE;z++)
              err += pow(c1(w,k,z) - c2(w,k,z), 2);

    return sqrt(err);
}

mat newParameters(mat par, mat SCALE)
{
    if ((rand() % 100)==0)
        par = generateParameters(SCALE); // to dla losowej inicjalizacji 1/100 ale marna szansa na trafieniew pelnej skali
    else
    {
#define   POS_RNG  4.0f
#define ANGLE_RNG 10.0f
#define  DAMP_RNG  2.0f
#define   AMP_RNG  3.0f
#define  BETA_RNG  0.05f
                                         // mutacja (random walk)
       for (int i=0;i<NPAR;i++)
           if ((rand() % 2)==0) //nie zawsze wszystkie poddajemy mutacji
           {
                par(i,0) += POS_RNG*( ((float)rand())/RAND_MAX) - POS_RNG/2.0;
                par(i,1) += POS_RNG*( ((float)rand())/RAND_MAX) - POS_RNG/2.0;
                par(i,2) += POS_RNG*( ((float)rand())/RAND_MAX) - POS_RNG/2.0;
                if (par(i,0) < SCALE(0,0))   par(i,0) = SCALE(0,0);
                if (par(i,1) < SCALE(0,1))   par(i,1) = SCALE(0,1);
                if (par(i,2) < SCALE(0,2))   par(i,2) = SCALE(0,2);
                if (par(i,0) > SCALE(1,0))   par(i,0) = SCALE(1,0);
                if (par(i,1) > SCALE(1,1))   par(i,1) = SCALE(1,1);
                if (par(i,2) > SCALE(1,2))   par(i,2) = SCALE(1,2);
 
                par(i,3) += ANGLE_RNG*( ((float)rand())/RAND_MAX) - ANGLE_RNG/2.0;
                par(i,4) += ANGLE_RNG*( ((float)rand())/RAND_MAX) - ANGLE_RNG/2.0;
                par(i,5) += ANGLE_RNG*( ((float)rand())/RAND_MAX) - ANGLE_RNG/2.0;
                if (par(i,3) < 0)            par(i,3) = par(i,3)+360.0;
                if (par(i,4) < 0)            par(i,4) = par(i,4)+360.0;
                if (par(i,5) < 0)            par(i,5) = par(i,5)+360.0;
                par(i,3) = fmod(par(i,3),360);
                par(i,4) = fmod(par(i,4),360);
                par(i,5) = fmod(par(i,5),360);

                par(i,6) += DAMP_RNG*( ((float)rand())/RAND_MAX) - DAMP_RNG/2.0;
                par(i,7) += DAMP_RNG*( ((float)rand())/RAND_MAX) - DAMP_RNG/2.0;
                par(i,8) += DAMP_RNG*( ((float)rand())/RAND_MAX) - DAMP_RNG/2.0;
                if (par(i,6) < SCALE(0,6))   par(i,6) = SCALE(0,6);
                if (par(i,7) < SCALE(0,7))   par(i,7) = SCALE(0,7);
                if (par(i,8) < SCALE(0,8))   par(i,8) = SCALE(0,8);
                if (par(i,6) > SCALE(1,6))   par(i,6) = SCALE(1,6);
                if (par(i,7) > SCALE(1,7))   par(i,7) = SCALE(1,7);
                if (par(i,8) > SCALE(1,8))   par(i,8) = SCALE(1,8);

                par(i,9) += AMP_RNG*( ((float)rand())/RAND_MAX) - AMP_RNG/2.0;
                if (par(i,9) < SCALE(0,9))   par(i,9) = SCALE(0,9);
                if (par(i,9) > SCALE(1,9))   par(i,9) = SCALE(1,9);

                par(i,10) += BETA_RNG*( ((float)rand())/RAND_MAX) - BETA_RNG/2.0;
                if (par(i,10) < SCALE(0,10))   par(i,10) = SCALE(0,10);
                if (par(i,10) > SCALE(1,10))   par(i,10) = SCALE(1,10);
       }
    }

    return par;
}

int main()
{
    cube V_ref     = cube(BSIZE,BSIZE,BSIZE,fill::zeros);
    cube V_best    = cube(BSIZE,BSIZE,BSIZE,fill::zeros);
    cube V_temp    = cube(BSIZE,BSIZE,BSIZE,fill::zeros);
    mat par        = mat(NPAR,11,fill::zeros);
    mat par_ref    = mat(NPAR,11,fill::zeros);
    mat par_best   = mat(NPAR,11,fill::zeros);
    mat par_temp   = mat(NPAR,11,fill::zeros);
    float err_best;

    mat SCALE = mat(2,11,fill::zeros);			//zakres wartosci parametrow dla generatora
    //MINIMALNE
    SCALE(0,0) = BSIZE/4.0;	//wiersz	m_x
    SCALE(0,1) = BSIZE/4.0;	//kolumna	m_y
    SCALE(0,2) = BSIZE/4.0;	//glebokosc	m_z
    SCALE(0,3) = 0;		//kat theta_x
    SCALE(0,4) = 0;		//kat theta_y
    SCALE(0,5) = 0;		//kat theta_z
    SCALE(0,6) = 5.0f;		//amplituda rozmiaru elipsoidy (skala) s_x
    SCALE(0,7) = 5.0f;		//amplituda rozmiaru elipsoidy (skala) s_y
    SCALE(0,8) = 5.0f;		//amplituda rozmiaru elipsoidy (skala) s_z
    SCALE(0,9) = 5.0f; 	//amplituda (czyli wplywa na maksymalna wartosc w wolumenie) A
    SCALE(0,10)= 0.5f; 	//beta 

    //MAKSYMALNE
    SCALE(1,0) = BSIZE*3.0/4.0;	//wiersz	m_x
    SCALE(1,1) = BSIZE*3.0/4.0;	//kolumna	m_y
    SCALE(1,2) = BSIZE*3.0/4.0;	//glebokosc	m_z
    SCALE(1,3) = 360.0;		//kat theta_x
    SCALE(1,4) = 360.0;		//kat theta_y
    SCALE(1,5) = 360.0;		//kat theta_z
    SCALE(1,6) = 10.0f;		//amplituda rozmiaru elipsoidy (skala) s_x
    SCALE(1,7) = 10.0f;		//amplituda rozmiaru elipsoidy (skala) s_y
    SCALE(1,8) = 10.0f;		//amplituda rozmiaru elipsoidy (skala) s_z
    SCALE(1,9) = 20.0f; 	//amplituda (czyli wplywa na maksymalna wartosc w wolumenie) A
    SCALE(1,10)= 1.5f; 	//beta 

    par=generateParameters(SCALE);

    cout << par << endl;

    srand((unsigned int)time(NULL));

    //referencyjny wolumen (poszukiwany)
    par_ref  = par;
    V_ref    = calculateVolumen(par_ref);
    //zaszumienie wolumenu (gauss do testow, w rzeczywsitym SPECT nie jest to gauss)
//  V_ref    = V_ref + 3.0f*randn<cube>(BSIZE,BSIZE,BSIZE);	//moze byc <0 ! trudno
//do pierwszych testow 0.0f lub skomentowac powyzsza linijke

    par_best = generateParameters(SCALE); //inicjalizacja najlepszych
    V_best   = calculateVolumen(par_best);

    err_best = calculateError(V_ref, V_best); //error

    cout << err_best << endl;

    float err;

    for (int n=0;n<N_MAX;n++) //ilosc iteracji
    {
        par_temp = newParameters(par_best, SCALE);
        V_temp   = calculateVolumen(par_temp);

        err = calculateError(V_ref, V_temp);
        if (err < err_best)
        {
           err_best = err;
           par_best = par_temp;
           V_best   = V_temp;
           cout << "n: " << n << " " << err_best << endl;
           cout << par_ref  << endl;
           cout << par_best << endl;  //niekoniecznie sa w tej samej kolejnosc co te wyzej
           cout << "------" << endl;
        }
    }
    return 0;
}

